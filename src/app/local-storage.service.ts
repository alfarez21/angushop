import { Injectable } from '@angular/core';

import * as _ from 'lodash';

@Injectable({
	providedIn: 'root'
})
export class LocalStorageService {

	constructor() { }

	addStorage(key,id){
		let listID = [];
		let sListID = this.getStorage(key);
		if(sListID){
			for(let sID of sListID){
				if(sID!=id) listID.push(sID)
			}
		}
		listID.push(id)		
		localStorage.setItem(key,JSON.stringify(listID))
	}
	getStorage(key){
		return JSON.parse(localStorage.getItem(key))
	}

	removeStorage(key,id){
		let sID = this.getStorage(key);
		let listID = _.filter(sID,e => e != id)
		localStorage.setItem(key,JSON.stringify(listID))
	}
}
