import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getProducts():Observable<any>{
    return this.http.get('../../assets/json/product.json');
  }
  
  getCategories():Observable<any>{
    return this.http.get('../../assets/json/category.json');
  }

  getColors():Observable<any>{
    return this.http.get('../../assets/json/color.json');
  }

  getSizes():Observable<any>{
    return this.http.get('../../assets/json/size.json');
  }
}
