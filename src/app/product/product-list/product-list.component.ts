import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LocalStorageService } from '../../local-storage.service';

@Component({
	selector: 'app-product-list',
	templateUrl: './product-list.component.html',
	styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

	@Input() products: any;
	@Input() currPage:number

	@Output() page = new EventEmitter();
	@Output() id = new EventEmitter();
  
	constructor( 
		private route: ActivatedRoute,
		public router: Router,
		private storage: LocalStorageService
	) { }

	ngOnInit() {}

	// Send Current Page to Parent Element
	currentPage(page){
		this.page.emit(page)
	}

	// Send Id to Parent
	removeWishList(id){
		this.id.emit(id);
	}

}
