import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillterProductComponent } from './fillter-product.component';

describe('FillterProductComponent', () => {
  let component: FillterProductComponent;
  let fixture: ComponentFixture<FillterProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillterProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillterProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
