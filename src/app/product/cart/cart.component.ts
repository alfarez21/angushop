import { Component, OnInit } from '@angular/core';

import * as _ from 'lodash';

import { ProductService } from '../product.service';
import { LocalStorageService } from '../../local-storage.service';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

	products:any;
	cartList:any;
	priceList = new Array();
	priceListOri = new Array();
	promo:number = 0;
	subTotal:number;
	totalPrice:number;
	promoCode = [
		{ code:"4234OPD", disconBy:"percent", discon:20 , minPrice:0 , desc:"Free 20%" },
		{ code:"12312B", disconBy:"price", discon:100 , minPrice:0, desc:"Free 100" },
	]

	constructor(
		private productService:ProductService,
		private storage:LocalStorageService
	) { }


	ngOnInit() {
		this.cartList = this.storage.getStorage('cart');
		this.getData();
	}
	
	// getting data
	getData(){
	
		let cartList = this.cartList;
		let arrId = [];
		let subTotal = []

		_.map(cartList,e=>arrId.push(e.id)) // push to arrID

		this.productService.getProducts().subscribe( res => {

			let products = res; // assign  data from service to local products var 
			this.products = _.filter(products,e=>_.includes(arrId,e.id)); // fillter and push local products var to global product var 
		
			_.map(this.products,e=>{
				this.priceList.push({id:e.id,TPrice:e.price}); //push to priceList array
				this.priceListOri.push({id:e.id,price:e.price}); //push to priceOri array
			})
		
			_.map(this.priceListOri,e=>{
				subTotal.push(e.price) //push to Subtotal array
			})
		
			this.setTotalPrice(subTotal) // update subtotal & totalprice
		});
	}

	// remove product from cartlist
	removeCart(id){
		let cartList = _.filter(this.cartList,e=>e.id!=id) // remove product selected
		localStorage.setItem('cart',JSON.stringify(cartList)) // update data cartlist
		this.ngOnInit(); // refresh component
	}

	// Update Subtotal and total price
	setTotalPrice(subTotal){
		this.subTotal = Number(_.sum(subTotal))
		this.totalPrice = this.subTotal-this.promo;
	}

	// Update cart product
	updateProduct(id,quan){

		// if quan not empty
		if(quan){
		
			let subTotal = new Array();
			let cartList = this.cartList;
			let priceList = this.priceList;
			let priceOri = _.clone(this.priceListOri); //clone this.priceListOri

			priceOri = _.filter(priceOri,{id:id}) // filter priceOri where pirce.id === id

			_.map(priceList,e=>{
				if(e.id==id) {
					(e.TPrice==0) ? e.TPrice+=priceOri[0].price: e.TPrice*=quan
				}
			})
			
			_.map(priceList,e=>subTotal.push(e.TPrice))
			this.setTotalPrice(subTotal)			
			this.priceList = priceList;
			localStorage.setItem('cart',JSON.stringify(cartList));
		}
	}

	getPromo(code){
		let promo = _.find(this.promoCode,e=> e.code === code)
		if(promo.minPrice <= this.subTotal){
			if(promo.disconBy=="percent") this.promo = this.subTotal*(promo.discon/100)
			if(promo.disconBy=="price") this.promo = promo.discon
			this.totalPrice = this.subTotal-this.promo;		
		}
		else{
			window.alert('Sorry, You cant use this Promo Code')
		}
 	}
}
