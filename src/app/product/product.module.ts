import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { NgxPaginationModule } from 'ngx-pagination';

import { ProductComponent } from './product.component';
import { FillterProductComponent } from './fillter-product/fillter-product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { RouterModule } from '@angular/router';
import { WishlistComponent } from './wishlist/wishlist.component';
import { LayoutModule } from '../layout/layout.module';
import { CartComponent } from './cart/cart.component';
import { CompareComponent } from './compare/compare.component';



@NgModule({
  declarations: [
    ProductListComponent, 
    FillterProductComponent, 
    ProductComponent, ProductDetailComponent, WishlistComponent, CartComponent, CompareComponent
  ],
  imports: [
    CommonModule,
    NgxPaginationModule,
    RouterModule,
    LayoutModule
  ],
  exports: [
    ProductListComponent,
    FillterProductComponent,
    WishlistComponent,
    ProductComponent,
    HttpClientModule
  ]
})
export class ProductModule { }
