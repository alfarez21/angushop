import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { WishlistComponent } from './product/wishlist/wishlist.component';
import { CartComponent } from './product/cart/cart.component';
import { CompareComponent } from './product/compare/compare.component';


const routes: Routes = [
  { path: "products" , component: ProductComponent },
  { path: "wishlist" , component: WishlistComponent },
  { path: "cart" , component: CartComponent },
  { path: "compare" , component: CompareComponent },
  { path: "products/p/:productSlug" , component: ProductDetailComponent },
  { path: "" , redirectTo: "products", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
