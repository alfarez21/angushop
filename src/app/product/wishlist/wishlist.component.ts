import { Component, OnInit } from '@angular/core';

import * as _ from 'lodash';

import { ProductService } from '../product.service';
import { LocalStorageService } from '../../local-storage.service';

@Component({
	selector: 'app-wishlist',
	templateUrl: './wishlist.component.html',
	styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {

	products:any;

	constructor(
		private storage:LocalStorageService,
		private productService: ProductService
	) { }

	ngOnInit() {
		this.getProducts(); 
	}


	getProducts(){
		let product:any[];
		let id = this.storage.getStorage('wishlist')
		this.productService.getProducts().subscribe(res => {
			product=res; 
			this.products = _.filter(product,e => _.includes(id,e.id))
		})
	}

	removeWishList(id){
		this.storage.removeStorage('wishlist',id);
		this.ngOnInit()
	}
}
