import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as _ from 'lodash';

import { ProductService } from '../product.service';
import { LocalStorageService } from '../../local-storage.service';

@Component({
	selector: 'app-product-detail',
	templateUrl: './product-detail.component.html',
	styleUrls: ['./product-detail.component.scss']
})

export class ProductDetailComponent implements OnInit {

	product:any;
	path:string;

	objCart = {
		id: null,
		quantity: 0
	}

	constructor(
		private productService: ProductService,
		private route:ActivatedRoute,
		private storage: LocalStorageService 
	) { }

	ngOnInit() {
		this.getProductBySlug();
	}

	// Gat Product by Slug Product
	getProductBySlug():void{

		let slug:string = this.route.snapshot.paramMap.get('productSlug'); // get product slug
		this.productService.getProducts().subscribe(res => { 
			let product = res; 
			this.product = _.find(product,{ slug : slug }) // find product by slug
			this.objCart.id = this.product.id;
			let list = this.storage.getStorage('cart');	
			if(list){
				_.map(list,e => {
					if(e.id==this.objCart.id) this.objCart.quantity = e.quantity
				})
			}
		});
	}


	// Add Product To wishlist
	addWishlist(id){
		this.storage.addStorage("wishlist",id);
	}

	// Add Product To wishlist
	addCompare(id){
		this.storage.addStorage("compare",id);
	}

	// Add Product To Cart
	addCart(){
		let id = this.objCart.id;
		let list = this.storage.getStorage('cart');
		if(list){
			let arrId = [];
			_.map(list,(e)=>{
				if(e.id===id) e.quantity++;
				arrId.push(e.id);
			})
			if(!_.includes(arrId,id)) {
				this.objCart.quantity++
				list.push(this.objCart)
			};
			localStorage.setItem('cart',JSON.stringify(list));
		}else{
			this.objCart.quantity++;
			this.storage.addStorage('cart',this.objCart)
		}
		this.ngOnInit();
	}

	// Alert
	succesMessage(message){
		window.alert(message);
	}
}
