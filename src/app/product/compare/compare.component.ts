import { Component, OnInit } from '@angular/core';

import * as _ from 'lodash';

import { ProductService } from '../product.service';
import { LocalStorageService } from '../../local-storage.service';

@Component({
	selector: 'app-compare',
	templateUrl: './compare.component.html',
	styleUrls: ['./compare.component.scss']
})
export class CompareComponent implements OnInit {

	products:any;
	start:number = 0;
	end:number = 3;

	constructor(
		private productService:ProductService,
		private storage:LocalStorageService
	) { }

	ngOnInit() {
		this.getProducts()
	}

	// Next Compare List
	next(){
		this.start++;
		this.end++;
	}

	// Prev Composer List
	prev(){
		this.start--;
		this.end--;
	}

	// get Product from service
	getProducts(){
		let listID = this.storage.getStorage('compare') // get compare list from local storage
		this.productService.getProducts().subscribe(res=>{
			let products = res // assign product to local product var
			this.products = _.filter(products,e=> _.includes(listID,e.id)) // filter product by list compare
		})
	}

	// remove product from compare
  	removeCompare(id){
		this.storage.removeStorage('compare',id) // remove 
		this.ngOnInit() // refresh component
	}

}
