import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as _ from 'lodash';

import { ProductService } from '../product.service';
import { del } from 'selenium-webdriver/http';


@Component({
    selector: 'app-fillter-product',
    templateUrl: './fillter-product.component.html',
    styleUrls: ['./fillter-product.component.scss']
})

export class FillterProductComponent implements OnInit {

    products:any;
    originalData:any;
    categories: any;
    colors: any;
    sizes: any;
    page:number;

    params: any = {
        search:null,
        category:null,
        price:null,
        color:null,
        size:null
    };

    constructor(
        private route:ActivatedRoute,
        private router:Router,
        private productService:ProductService
    ) { }

    ngOnInit() {
        this.getProduct();
        this.getCategories();
        this.getSizes();
        this.getColors();
        this.getParams();
    }

    // Product Filter
    filterProduct(param){


        // clone param value
        let cloneParam = _.clone(param);

        // Checking Clone Param
        _.map(cloneParam,(x,i)=>{

            // delete size,price,search & page in Clone Param
            if(i === 'size' || i === 'price' || i === "search" || i === "page") delete cloneParam[i];
            
            // delete params if has null
            if(!x) delete cloneParam[i];

            // Replace  '-' to ' ' in category
            if(i === 'category'){
                while(_.includes(cloneParam[i],"-")){
                    cloneParam[i] = _.replace(cloneParam[i],'-',' ');
                }
            }
        });
        // filter by Clone Params
        this.products = _.filter(this.originalData,cloneParam);

        // filter by Size
        if(param['size']){
            let size = param['size'].split`-`.map(x=>+x)
            this.products = _.filter(this.products,(x)=> _.includes(size,x.size));  
        }

        // filter By Price
        if(param['price']){
            let price = Number(param['price']);
            this.products = _.filter(this.products, (x)=> x.price <= price);
        }

        // filter by search
        if(param['search']){
            this.products = _.filter(this.products, (x)=> _.includes(x.slug, param.search) );
        }

    }

    // Get Query Params then push to params variable
    getParams(){
        let sizes = new Array()
        let params = this.route.snapshot.queryParams; // get params
        let ParamKey = Object.keys(params);
        let param = new Object;

        _.map(params,(x,i)=>param[i]=x)
        _.map(param,(v,k)=>{
            if(k === "page") this.page = v
            if(k === "size") {
                sizes = v.split`-`.map(v=>+v)
            }
        })
        this.params = param

        this.productService.getSizes().subscribe(res => {
            this.sizes = res;
            _.map(this.sizes,e=>{
                _.map(sizes,i=>{
                    if(i==e.size) e.check  = true;
                })
            })
        })

        
    }

    // Update Params Function
    updateParams(param,value): void{

        this.params[param] = value; // update params var
        let obj = this.params;

        // get property an object
        let params = Object.getOwnPropertyNames(obj)

        // Delete propery has null value
        params.map((val) =>{ 
            if(!obj[val]) delete obj[val];
        });

        // Navigate to the Route with query param 
        this.router.navigate(['/products'],{ queryParams: obj })
    }

  
    // get product name 
    searchName(data):void{

        // Replace space to strip if included
        while(_.includes(data,' ')){
            data = _.replace(data, ' ', '-')
        }

        this.updateParams('search',data); // push to params var
        this.filterProduct(this.params);  // filter
            
    }

    // get product color  
    searchColor(data):void{
 
        if(data==this.params.color){
            this.updateParams('color',null) // if color exist in params var push null value
        }else(
            this.updateParams('color',data) // if color doest exist in params var push selected color
        )
        this.filterProduct(this.params); // filter
    }

    // get product category
    searchCategory(data):void{

        // Replace space to strip if included
        while(_.includes(data,' ')){
            data = _.replace(data, ' ', '-')
        }

        if(data!="all"){
            this.updateParams('category',data); // if not all category push categoru selected to params var
        }
        else{
            this.updateParams('category',null); // if all category set category to null
        }
        this.filterProduct(this.params); //filter

    }

    // filter by price
    searchPrice(data):void{

        if(data!=0){
            this.updateParams('price',data); // if not 0 push price to params var
        }
        else{
            this.updateParams('price',null); // if 0 set price to null
        }
        this.filterProduct(this.params); // filter
    }

    // filter by price
    searchSize():void{

        let paramSize =  new Array(); // Create null array

        this.sizes.forEach((e) => {
            if(e.check){
                paramSize.push(e.size); // if check true push size to paramSize var  
            }
        });

        let param = paramSize.join('-'); // Covert array to string
        this.updateParams('size',param); // Push size to params var
        this.filterProduct(this.params); //  filter

    }

    // Get Products
    getProduct(){
        this.productService.getProducts().subscribe(res =>{ 
            this.products = res;
            this.originalData = res;
            this.filterProduct(this.params);
        })
    }

    // Get Categories
    getCategories():void{
        this.productService.getCategories().subscribe(res => this.categories = res)
    }

    // Get Categories
    getColors():void{
        let colors;
        this.productService.getColors().subscribe(res => this.colors = res )
    }

    // Get Categories
    getSizes():void{
        this.productService.getSizes().subscribe(res => {
            this.sizes = res
        })
    }


}
